package com.example.simpleapp.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Osoba {

    @Id
    private Integer id;

    private String name;

}
