package com.example.simpleapp.repositories;

import com.example.simpleapp.entities.Osoba;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OsobaRepository extends JpaRepository<Osoba, Integer> {
}
