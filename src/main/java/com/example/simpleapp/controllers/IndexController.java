package com.example.simpleapp.controllers;

import com.example.simpleapp.repositories.OsobaRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class IndexController {

    private OsobaRepository osobaRepository;


    @GetMapping("/osoba")
    public String getOsoba(){
        return this.osobaRepository.findAll().get(0).getName();
    }

    @GetMapping("/")
    public String getIndex(){

        return "ahoj světe";
    }
}
